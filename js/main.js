
$(document).ready(function () {

	$(document).on('click','.icon-toggle-menu-child',function(){
		$(this).toggleClass("is-active");
		$(".box-menu-child .icon-toggle-menu-child").toggleClass("active");
		$(this).siblings('.box-menu-child').toggleClass('active');
		$(this).parent('.wrap-menu-child').toggleClass('active');
		// 
		// $('body').toggleClass('overlay');
		$('.menu').toggleClass('active');
		$('header#header').toggleClass('active-menu');
		$('main#main').toggleClass('active-menu');
		$('footer#footer').toggleClass('active-menu');
	});
	$(document).mouseup(function (e) {
		var container = $(".box-menu-child");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			if ($(".icon-toggle-menu-child").hasClass('is-active')) {
				$(".icon-toggle-menu-child").removeClass("is-active");
				$(".icon-toggle-menu-child").removeClass('active');
				$('.box-menu-child').removeClass('active');
				$('.wrap-menu-child').removeClass('active');
			}
			// $('body').removeClass('overlay');
			$('.menu').removeClass('active');
			$('header#header').removeClass('active-menu');
			$('main#main').removeClass('active-menu');
			$('footer#footer').removeClass('active-menu');

		}
	});
	$(document).on('click','footer .icon-toggle-menu-child',function(){
		$('footer#footer').removeClass('active-menu');
	});

	$(document).on('click','.menu-header-main .box-menu-child .icon-toggle-menu-child',function(){
		$(this).parent('.menu-header-main .box-menu-child').toggleClass('active');
		$(".menu-header-main .box-menu-child .icon-toggle-menu-child").toggleClass('active');
		$(".menu-header-main .icon-toggle-menu-child").toggleClass('is-active');
		$('.menu-header-main .wrap-menu-child').toggleClass('active');
	});
	$(document).on('click','.menu-footer-main .box-menu-child .icon-toggle-menu-child',function(){
		$(this).parent('.menu-footer-main .box-menu-child').toggleClass('active');
		$(".menu-footer-main .box-menu-child .icon-toggle-menu-child").toggleClass('active');
		$(".menu-footer-main .icon-toggle-menu-child").toggleClass('is-active');
		$('.menu-footer-main .wrap-menu-child').toggleClass('active');
	});
	$(document).on('click','.box-menu-child .icon-toggle-menu-child',function(){
		$(this).removeClass('active');
	});

	// menu
	// $(".icon-toggle-menu-child").click(function () {
	// 	$('.menu .icon-bar').toggleClass('active');
	// 	$('body').toggleClass('overlay');
	// 	$('nav#nav').toggleClass('active');
	// 	$('header#header').toggleClass('active-menu');
	// 	$('main#main').toggleClass('active-menu');
	// 	$('footer#footer').toggleClass('active-menu');
	// });
	// $(document).mouseup(function (e) {
	// 	var container = $(".box-menu-child");
	// 	if (!container.is(e.target) &&
	// 		container.has(e.target).length === 0) {
	// 		$('.menu .icon-bar').removeClass('active');
	// 		$('body').removeClass('overlay');
	// 		$('.menu').removeClass('active');
	// 		$('header#header').removeClass('active-menu');
	// 		$('main#main').removeClass('active-menu');
	// 		$('footer#footer').removeClass('active-menu');
	// 	}
	// });


	// customenu
	function ResizeMemuMain() {
		var arrClassParent = ['menu-header-main', 'menu-footer-main'];
		for (let m = 0; m < arrClassParent.length; m++) {
			$("."+ arrClassParent[m] + " .wrap-menu-child>.box-menu-child>.item").appendTo("."+ arrClassParent[m] + " .box-menu-display");
			$("."+ arrClassParent[m] + " .wrap-menu-child").remove();
			var win = $(window).width();
			if(win > 1024){
			var fixWidth = $("."+ arrClassParent[m] + " .box-menu-display").outerWidth() - 60;
			var listLi = $("."+ arrClassParent[m] + " .box-menu-display>.item");
			var cWidth = 0;
			var iMax = 0;
			for (var i = 0; i < listLi.length; i++) {
				cWidth += $(listLi[i]).outerWidth();
				if (cWidth > fixWidth) {
					console.log(cWidth > fixWidth);
					console.log(listLi[0]);
					iMax = i;
					break;
				}
			}
			if (iMax > 0) {
				var li2 = "<div class='wrap-menu-child'><div class='icon-toggle-menu-child'><span></span></div><div class='box-menu-child'></div></div>";
				$(li2).appendTo("."+ arrClassParent[m] + " .box-menu-display");

				var countMore = 0;
				for (var i = iMax; i < listLi.length; i++) {
					$(listLi[i]).appendTo("."+ arrClassParent[m] + " .box-menu-child");
				}
			}
			}else{
				var listLi = $("."+ arrClassParent[m] + " .box-menu-display>.item");
				var li2 = "<div class='wrap-menu-child'><div class='icon-toggle-menu-child'><span></span></div><div class='box-menu-child'></div></div>";
				$(li2).appendTo("."+ arrClassParent[m] + " .box-menu-display");
				for (var i = 1; i < listLi.length; i++) {
					$(listLi[i]).appendTo("."+ arrClassParent[m] + " .box-menu-child");
				}
			}
		}
	}

	ResizeMemuMain();

	// submenu
	$(".icon-toggle-menu").click(function () {
		$(this).toggleClass('active');
		$(this).siblings('.menu-child').toggleClass('active');
		$(this).siblings('.menu-child').slideToggle();
	});
	var win = $(window).width();
	if(win < 1024){
		$('.box-menu-child').append("<div class='icon-toggle-menu-child'><span></span></div>")
	}
	$(window).resize(function() {
		ResizeMemuMain();
	});


	// scroll top 
	$('.scroll-top').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 500)
	});
	var offset = 600,
		$back_to_top = $('.scroll-top');
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('visible-top') : $back_to_top.removeClass('visible-top');
	});

	// cover size text 

	var count = 16;
	$("#minus").click(function () {
		if (count > 10) {
			count--;
			$('.wrap-blog-detail-main *').css('font-size', +count + 'px');
		}
	});
	$("#plus").click(function () {
		if (count < 25) {
			count++;
			$('.wrap-blog-detail-main *').css('font-size', +count + 'px');
		}
	});
	$('.cover-size span:first-child').click(function () {
		$('.wrap-blog-detail-main *').css('font-size', '16px');
	});


	// get date now 
	function getTime() {
		var objToday = new Date(),
			weekday = new Array('Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'),
			dayOfWeek = weekday[objToday.getDay()],
			dayOfMonth = today + (objToday.getDate() < 10) ? '0' + objToday.getDate() : objToday.getDate(),
			months = new Array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'),
			curMonth = months[objToday.getMonth()],
			curYear = objToday.getFullYear(),
			curHour = objToday.getHours() < 10 ? "0" +objToday.getHours() : objToday.getHours(),
			curMinute = objToday.getMinutes() < 10 ? "0" + objToday.getMinutes() : objToday.getMinutes(),
			curSeconds = objToday.getSeconds() < 10 ? "0" + objToday.getSeconds() : objToday.getSeconds();
		var today = curHour + ":" + curMinute + ", " + dayOfWeek + ", ngày " + dayOfMonth + "/" + curMonth + "/" + curYear;
		document.getElementById('date-now').innerHTML = today;
	}
	setInterval(getTime, 1000);


	// tabs right

	$(".toggle-tab-item").click(function () {
		$(this).siblings('.box-tab-child').slideToggle();
		$(this).toggleClass('active');
	});


});

// slide 
$('.slide-libary-toppage').owlCarousel({
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 0,
	responsive: {
		0: {
			items: 1,
			loop: ($('.slide-libary-toppage .item').length > 1) ? true : false,
		},
		0: {
			items: 1,
			loop: ($('.slide-libary-toppage .item').length > 1) ? true : false,
		},
		768: {
			items: 2,
			margin:15,
			loop: ($('.slide-libary-toppage .item').length > 2) ? true : false,
		},
		992: {
			items: 1,
			loop: ($('.slide-libary-toppage .item').length > 1) ? true : false,
		},
	}
});
$('.slide-magazine').owlCarousel({
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 15,
	responsive: {
		0: {
			items: 2,
			loop: ($('.slide-magazine .item').length > 2) ? true : false,
		},
		540: {
			items: 3,
			loop: ($('.slide-magazine .item').length > 3) ? true : false,
		},
		667: {
			items: 4,
			loop: ($('.slide-magazine .item').length > 4) ? true : false,
		},
		768: {
			items: 5,
			loop: ($('.slide-magazine .item').length > 5) ? true : false,
		},
		992: {
			items: 2,
			loop: ($('.slide-magazine .item').length > 2) ? true : false,
		},
	}
});
$('#video-hot-main').owlCarousel({
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 15,
	responsive: {
		0: {
			items: 1,
			loop: ($('#video-hot-main .item').length > 1) ? true : false,
		}
	}
});

$('#slide-links-website').owlCarousel({
	nav: true,
	autoplay: true,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	margin: 20,
	responsive: {
		0: {
			items: 1,
			loop: ($('.slide-links .item').length > 1) ? true : false,
		},
		428: {
			items: 2,
			loop: ($('.slide-links .item').length > 2) ? true : false,
		},
		769: {
			items: 3,
			loop: ($('.slide-links .item').length > 3) ? true : false,
		},
		1025: {
			items: 4,
			loop: ($('.slide-links .item').length > 4) ? true : false,
		},
		1081: {
			items: 5,
			loop: ($('.slide-links .item').length > 5) ? true : false,
		},
		1200: {
			items: 6,
			loop: ($('.slide-links .item').length > 6) ? true : false,
		},
	}
});

// // fix header 
// var prevScrollpos = window.pageYOffset;
// window.onscroll = function() {
// 	var currentScrollPos = window.pageYOffset;
// 	if (prevScrollpos > currentScrollPos) {
// 		$("#header").css({
// 			"position": "sticky",
// 			"top": 0,
// 		});
// 		$("#header").addClass('active');
// 	} else {
// 		$("#header").css({
// 			"top": "-140px",
// 		});
// 		$("#header").removeClass('active');
// 	}
// 	prevScrollpos = currentScrollPos;
// }

// $(window).on('scroll', function () {
// 	if ($(window).scrollTop()) {
// 		$('#header:not(.header-project-detail-not-scroll)').addClass('active');
// 	} else {
// 		$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
// 	};
// });




function resizeImage() {
	let arrClass = [
		{ class: 'cover-size-image-hot', number: (332 / 590) }, //0.5627118644067797
		{ class: 'cover-image-size-banner', number: (181 / 285) }, //0.6350877192982456
		{ class: 'cover-size-1', number: (56 / 98) }, //0.5714285714285714
		{ class: 'cover-size-2', number: (159 / 260) }, //0.6115384615384615
		{ class: 'cover-size-3', number: (76 / 136) }, //0.5588235294117647
		{ class: 'cover-size-4', number: (262 / 450) }, //0.5822222222222222
		{ class: 'cover-size-5', number: (121 / 216) }, //0.5601851851851852
		{ class: 'cover-size-6', number: (210 / 373) }, //0.5630026809651475
		{ class: 'cover-size-7', number: (95 / 170) }, //0.6724137931034483
		{ class: 'cover-size-8', number: (163 / 245) }, //0.6653061224489796
		{ class: 'cover-size-9', number: (77 / 115) }, //0.6695652173913043
		{ class: 'cover-size-10', number: (61 / 98) }, //0.6224489795918367
		{ class: 'cover-size-11', number: (151 / 270) }, //0.6224489795918367
		{ class: 'cover-size-12', number: (67 / 120) }, //0.5583333333333333
		{ class: 'cover-size-13', number: (105 / 187) }, //0.5614973262032086
		{ class: 'cover-size-14', number: (56 / 100) }, //0.56
		{ class: 'cover-size-15', number: (171 / 280) }, //0.6107142857142857
		{ class: 'cover-size-16', number: (80 / 244) }, //
		{ class: 'cover-size-17', number: (123 / 220) }, //
		{ class: 'cover-size-18', number: (78 / 140) }, //
		{ class: 'cover-size-19', number: (181 / 286) }, //0.6328671328671329
		{ class: 'cover-size-20', number: (151 / 107) }, //
		{ class: 'cover-size-21', number: (138 / 245) }, //
		{ class: 'cover-size-22', number: (44 / 78) }, //
		{ class: 'cover-size-23', number: (168 / 300) }, //
		{ class: 'cover-size-24', number: (181 / 272) }, //
		{ class: 'cover-size-25', number: (100 / 150) }, //
		{ class: 'cover-size-26', number: (380 / 271) }, //
		{ class: 'cover-size-27', number: (480 / 855) }, //
		{ class: 'cover-size-28', number: (278 / 417) }, //
		{ class: 'cover-size-29', number: (570 / 855) }, //
	];
	for (let i = 0; i < arrClass.length; i++) {
		let width = $("." + arrClass[i]['class']).width();
		$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		// console.log(width);
		// console.log(arrClass);
		// console.log(width*arrClass[i]['number']);
	}
}
resizeImage();
$(window).on('resize', function () {
	resizeImage();
});

// active youtube
function activeUrlVideo(attr) {
	var id_video = $(attr).data('url');
	var symbol = $("#video-main")[0].src;
	$("#video-main")[0].src = 'https://www.youtube.com/embed/' + id_video + '?autoplay=1';
}
function closeVideo() {
	var symbol = $("#video-main")[0].src;
	var arr = symbol.split('embed/');
	$("#video-main")[0].src = arr[0] + 'embed/';
}
$(".active-video").click(function () {
	activeUrlVideo(this);
});
$(".close").click(function () {
	closeVideo();
});
$(document).mouseup(function (e) {
	var container = $("#video-youtube .wrap-video-youtube");
	if (!container.is(e.target) &&
		container.has(e.target).length === 0) {
		if ($('.modal').hasClass('show')) {
			closeVideo();
		}
	}
});


